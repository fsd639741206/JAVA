package Day10;

public class Demo02 {
	
	public static int getVowelsCount(String str){
		int vc = 0;
		str = str.toLowerCase();
		for(int i = 0; i<str.length();i++){
			char ch = str.charAt(i);
			if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o'||ch == 'u'){
				vc++;
			}
		}
		return vc;
	}
	public static void main(String[] args) {
		System.out.println(getVowelsCount("aeiou"));
		System.out.println(getVowelsCount("Welcome"));
		System.out.println(getVowelsCount("TalentSprint"));
		System.out.println(getVowelsCount("sky"));
	}

}