package Day09;

public class Assig2 {
    public static void main(String[] args) {
        int[] input = {1, 2, 3, 1, 2, 4, 6};
        printDuplicateValues(input);
    }

    public static void printDuplicateValues(int[] input) {
        int[] count = new int[input.length];
        boolean[] visited = new boolean[input.length];

        // Count occurrences of each element
        for (int i = 0; i < input.length; i++) {
            if (!visited[i]) {
                int frequency = 1;
                for (int j = i + 1; j < input.length; j++) {
                    if (input[i] == input[j]) {
                        frequency++;
                        visited[j] = true;
                    }
                }
                count[i] = frequency;
            }
        }

        // Print duplicate values with their occurrences
        System.out.println("---");
        for (int i = 0; i < input.length; i++) {
            if (count[i] > 1) {
                System.out.println(input[i] + " : " + count[i]);
            }
        }
    }
}