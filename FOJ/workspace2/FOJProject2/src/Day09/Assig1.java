package Day09;

import java.util.Arrays;

public class Assig1 {
    public static void main(String[] args) {
        int[] input = {1, 2, 3, 1, 2, 4, 6};
        int[] result = getNonDuplicateValues(input);
        System.out.print("Output: ");
        for (int num : result) {
            System.out.print(num + " ");
        }
    }

    public static int[] getNonDuplicateValues(int[] input) {
        int[] result = new int[input.length];
        int currentIndex = 0;

        for (int i = 0; i < input.length; i++) {
            boolean duplicate = false;
            for (int j = 0; j < currentIndex; j++) {
                if (input[i] == result[j]) {
                    duplicate = true;
                    break;
                }
            }
            if (!duplicate) {
                result[currentIndex] = input[i];
                currentIndex++;
            }
        }

        return Arrays.copyOf(result, currentIndex);
    }
}