package Day08;

public class LinearSearch {
    public static int search(int arr[],int num){
    	for(int i=0;i<arr.length-1;i++){
    		if(num == arr[i]){
    			return i;
    		}
    	}
    	return -1;
    }
	public static void main(String[] args) {
		
		int arr[] = {30, 10, 20, 8, 25};
		System.out.println(search(arr, 99));
		System.out.println(search(arr, 10));
		System.out.println(search(arr, 20));
	}

}