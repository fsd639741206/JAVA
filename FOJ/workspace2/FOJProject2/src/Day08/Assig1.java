package Day08; 
import java.util.Scanner;

public class Assig1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int num = scanner.nextInt();

		if (isAdamNumber(num)) {
			System.out.println(num + " is an Adam number.");
		} else {
			System.out.println(num + " is not an Adam number.");
		}
	}


	public static boolean isAdamNumber(int num) {

		int square = num * num;


		int reversedSquare = reverseNumber(square);


		int reversedSquareSquared = reversedSquare * reversedSquare;


		return reversedSquareSquared == square;
	}


	public static int reverseNumber(int num) {
		int reversed = 0;
		while (num != 0) {
			int digit = num % 10;
			reversed = reversed * 10 + digit;
			num /= 10;
		}
		return reversed;
	}
}