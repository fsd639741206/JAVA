package Day08;

public class Bubblesort {
    public static void main(String[] args) {
        
        int[][] twoDArray = {
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12}
        };

        
        System.out.println("The 2D array:");
        printArray(twoDArray);

        
        int element = twoDArray[1][2];
        System.out.println("\nElement at row 1, column 2: " + element);

        
        twoDArray[2][3] = 20;

       
        System.out.println("\nThe modified 2D array:");
        printArray(twoDArray);
    }

    
    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}