package Day08;

import java.util.Scanner;

public class Assig3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the starting number: ");
		int start = scanner.nextInt();

		System.out.println("Collatz sequence for " + start + ":");
		generateCollatz(start);
	}

	public static void generateCollatz(int n) {
		while (n != 1) {
			System.out.print(n + " -> ");
			if (n % 2 == 0) {
				n /= 2;
			} else {
				n = 3 * n + 1;
			}
		}
		System.out.println(1);
	}
}