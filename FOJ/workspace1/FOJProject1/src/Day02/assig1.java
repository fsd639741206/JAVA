
package Day02;

public class assig1 {
    public static double calculateArea(double radius) {
        return Math.PI * radius * radius;
    }
    
    public static void main(String[] args) {
        double radius = 5;
        double area = calculateArea(radius);
        System.out.println("Area of the circle with radius " + radius + " is: " + area);
    }
}