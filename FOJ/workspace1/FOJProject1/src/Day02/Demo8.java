package Day02;

public class Demo8 {

	public static void main(String[] args) {
		String s="25";
		byte b =Byte.parseByte(s);      // String to any
		short sh=Short.parseShort(s);  //String to short
		int i  = Integer.parseInt(s); //string to int
		long l = Long.parseLong(s);  //String to long
		
		//Lower to Higher
		System.out.println("String s :" + s);
		System.out.println("byte s :" + b);
		System.out.println("short sh :" + sh);
		System.out.println("int i :" + i);
		System.out.println("long l :" + l);
		System.out.println();
		
		l = 45;
		i = (int) l;        //Type Casting int
		sh = (short) i;    //Type Casting Short 
		b = (byte) sh;    //Type Casting byte
		s = b + "";      //Converting  to String (Any to String)
		
		//Higher to Lower
		System.out.println("String s :" + s);
		System.out.println("byte s :" + b);
		System.out.println("short sh :" + sh);
		System.out.println("int i :" + i);
		System.out.println("long l :" + l);
		System.out.println();
		


	}

}
