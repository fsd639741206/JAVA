package Day02;

public class Demo4 {

	public static void main(String[] args) {
		int num1 = 10; int num2=10;
		
		System.out.println("num1 ="+num1+"\nnum2 ="+num2+"\n");
		System.out.println("Performing pre increment and decrement");
		num1 = ++num2;
		System.out.println("num1 ="+num1+"\nnum2 ="+num2+"\n");
		num1 = --num2;
		System.out.println("num1 ="+num1+"\nnum2 ="+num2+"\n");
		System.out.println("Performing post increment and decrement");
		num1 = num2++;
		System.out.println("num1 ="+num1+"\nnum2 ="+num2+"\n");
		num1 = num2--;
		System.out.println("num1 ="+num1+"\nnum2 ="+num2+"\n");
	}
}