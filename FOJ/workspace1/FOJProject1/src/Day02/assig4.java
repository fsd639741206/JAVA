package Day02;

public class assig4 {
    public static int nextMultiple(int num) {
        return ((num / 100) + 1) * 100;
    }
    
    public static void main(String[] args) {
        int num = 257;
        int nextMultiple = nextMultiple(num);
        System.out.println("Next multiple of 100 after " + num + " is: " + nextMultiple);
    }
}
