package Day05;
public class Assig1 {
    public static void main(String[] args) {
        int size = 5;
        
        // Upper part of the pattern
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - i; j++) {
                System.out.print("* ");
            }
            for (int j = 0; j < 2 * i; j++) {
                System.out.print("  ");
            }
            for (int j = 0; j < size - i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
        
//        // Middle line
//        for (int i = 0; i < 2 * size; i++) {
//            System.out.print("* ");
//        }
//        System.out.println();
        
        // Lower part of the pattern
        for (int i = size - 1; i >= 0; i--) {
            for (int j = 0; j < size - i; j++) {
                System.out.print("* ");
            }
            for (int j = 0; j < 2 * i; j++) {
                System.out.print("  ");
            }
            for (int j = 0; j < size - i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
