package Day04;
public class assig3 {
	
	public static double calculateEMI(double principal, double downPayment, double interestRate, int months) {
		double loanAmount = principal - downPayment;

		double monthlyInterestRate = interestRate / 100 / 12;

		double emi = (loanAmount * monthlyInterestRate) / (1 - Math.pow(1 + monthlyInterestRate, -months));

		return emi;
	}

	public static void main(String[] args) {
		double carPrice = 850000;
		double downPayment = 150000; 
		double interestRate = 12; 
		int loanTerm = 48;
		double emi = calculateEMI(carPrice, downPayment, interestRate, loanTerm);
		System.out.println("EMI for the car loan: Rs. " + Math.round(emi * 100.0) / 100.0);
	}
}