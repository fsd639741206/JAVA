package Day04;
 
//Check Even or Odd 

public class Assig2 {
    public static int checkevenodd(int num){
    	if(num<0)
    		return -1;
    	else if(num%2 == 0)
    		return 1;
    	else
    		return 0;
    }
	public static void main(String[] args) {
		
		System.out.println(checkevenodd(89));
		System.out.println(checkevenodd(58));
		System.out.println(checkevenodd(12));
		System.out.println(checkevenodd(-85));
		System.out.println(checkevenodd(7));
	}

}