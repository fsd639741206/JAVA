package Day04;

//Distance 

public class Assig4{

	public static void main(String[] args) {
		double distanceInMeters = 160000;
		int hours = 6;
		int minutes = 39;
		int seconds = 55;
		double totalTimeInHours = hours + (minutes / 60.0) + (seconds / 3600.0);

		double speedInMetersPerSecond = distanceInMeters / (hours * 3600 + minutes * 60 + seconds);
		double speedInMetersPerHour = distanceInMeters / totalTimeInHours;
		double speedInMilesPerHour = (distanceInMeters / 1609) / totalTimeInHours;

		System.out.println("Speed in meters per second: " + speedInMetersPerSecond);
		System.out.println("Speed in meters per hour: " + speedInMetersPerHour);
		System.out.println("Speed in miles per hour: " + speedInMilesPerHour);

	}

}