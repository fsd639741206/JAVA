package Day03;

//FuzzBizz

public class Assig2 {

	public static void method1(int num){
		if(num % 3 == 0 && num % 5 == 0)
			System.out.println("FuzzBizz");
		else if(num % 3 == 0)
			System.out.println("Fuzz");
		else if(num % 5 == 0)
			System.out.println("Bizz");
		else
			System.out.println(num);

	}

	public static void main(String[] args) {
		method1(12);
	}

}