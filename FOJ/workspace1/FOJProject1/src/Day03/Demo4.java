package Day03;

public class Demo4 {

	public static int greatestOfTwoDigit(int num1,int num2){
		if(num1 > num2){
			return num1;
		}else{
			return num2;
		}
	}

	public static void main(String[] args) {
		System.out.println(greatestOfTwoDigit(5,8));

	}

}